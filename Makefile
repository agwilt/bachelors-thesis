.PHONY: de en
en: thesis-en.pdf
de: thesis.pdf

thesis.pdf: thesis.tex sources.bib commands.tex tikz_commands.tex images/*.pdf
	pdflatex thesis.tex
	bibtex thesis
	pdflatex thesis.tex
	pdflatex thesis.tex

thesis-en.pdf: thesis-en.tex sources.bib commands-en.tex tikz_commands.tex images/*.pdf
	pdflatex thesis-en.tex
	bibtex thesis-en
	pdflatex thesis-en.tex
	pdflatex thesis-en.tex

.PHONY: tidy clean_all
tidy:
	rm -fv *.aux *.log *.toc *.out *.bbl *.blg
clean_all: tidy
	rm -fv *.dvi *.pdf *.man
