# Introduction

## What is a FET?

## What is a leaf cell? --> What is a PC track?

## Touch on alignment

# Preliminaries

## Definitions

Fet, Net, etc.

## Single Stack Placement

Define configuration graph, mention eulerian trail algorithm

## Alignment Function

Alignment more important than width for practical use, etc.

## Define central problems

* Entscheidungsproblem
* Optimierungsproblem

# Word-aligned eulerian trails

* Define problem, assumptions, etc.: directed graph, one stack has exactly one perfect placement, no swapping
* Kupferman, Vardi => NP-hard
* Point out: Also NP-hard for undirected graphs, by double number of colours
* Point out: Also NP-hard for only 2 colours, undir+dir

## lambda-WAET

### linear(?) algorithm for 2-WAET

### lambda > 2???????

# Approximation
